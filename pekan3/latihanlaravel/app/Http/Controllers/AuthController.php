<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.form');
    }

    public function daftar(Request $request)
    {
        $nama = $request['nama'];
        return view('halaman.welcome', compact('nama'));
    }
}
