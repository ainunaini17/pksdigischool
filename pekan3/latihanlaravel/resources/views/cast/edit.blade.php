@extends('layout.master')

@section('judul')
    Halaman Edit Cast {{$cast->nama}}
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="exampleInputEmail1">Nama Cast</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control" placeholder="Nama Cast">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label for="exampleInputPassword1">Umur</label>
      <input type="text" name="umur" value="{{$cast->umur}}" class="form-control" placeholder="Umur">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label for="exampleInputPassword1">Bio</label>
      <textarea name="bio" class="form-control" id="bio_cast" cols="10" rows="5">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection