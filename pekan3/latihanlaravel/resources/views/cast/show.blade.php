@extends('layout.master')

@section('judul')
    Detail Cast {{$cast->nama}}
@endsection

@section('content')

<h3>{{$cast->nama}}</h3>
<p>Umur : {{$cast->umur}}</p>
<p>Bio : {{$cast->bio}}</p>

@endsection