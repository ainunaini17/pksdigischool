@extends('layout.master')

@section('judul')
    Halaman Form
@endsection

@section('content')
    <form action="/welcome" method="POST">
        @csrf
        <div>
            <h1>Buat Account Baru!</h1>
            <h2>Sign Up Form</h2>
        </div>
        <div>
            <label for="nama1">First Name : </label>
            <br><br>
            <input type="text" name="nama" id="nama1">
        </div>
        <br>
        <div>
            <label for="nama2">Last Name : </label>
            <br><br>
            <input type="text" id="nama2">
        </div>
        <br>
        <div>
            <label>Gender : </label>
            <br><br>
            <input type="radio" name="gender" value="Male">Male <br>
            <input type="radio" name="gender" value="Female">Female <br>
            <input type="radio" name="gender" value="Other">Other <br>
        </div>
        <br>
        <div>
            <label>Nationality : </label>
            <br><br>
            <select name="nationality">
                <option value="Indonesian">Indonesia</option>
                <option value="American">America</option>
            </select>
        </div>
        <br>
        <div>
            <label>Language Spoken : </label>
            <br><br>
            <input type="checkbox" name="bahasa" value="B.Indo">Bahasa Indonesia <br>
            <input type="checkbox" name="bahasa" value="English">English <br>
            <input type="checkbox" name="bahasa" value="Other">Other <br>
        </div>
        <br>
        <div>
            <label for="bio">Bio : </label>
            <br><br>
            <textarea cols="50" rows="7"></textarea>
        </div>
        <div>
            <input type="submit" value="Sign Up">
            <!-- <button><a href="/welcome">Sign Up</a></button> -->
        </div>
    </form>
@endsection