@extends('layout.master')

@section('judul')
    <h1>Media Online</h1>
@endsection

@section('content')
    <div>
        <h2>Social Media Developer</h2>
        <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    </div>
    <div>
        <h3>Benefit Join Di Media Online</h3>
        <ul>
            <li>Mendapatkan motivasi dari sesama developer</li>
            <li>Sharing Knowlenge</li>
            <li>Dibuat oleh calon developer terbaik</li>
        </ul>
    </div>
    <div>
        <h2>Cara Bergabung ke Media Online</h2>
        <ol>
            <li>Mengunjungi Website ini</li>
            <li>Mendaftarkan <a href="/register">Form Sign Up</a>
            <li>Selesai</li>
        </ol>
    </div>
@endsection
    
